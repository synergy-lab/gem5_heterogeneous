/*
 * Copyright (c) 2011-2015 Advanced Micro Devices, Inc.
 * All rights reserved.
 *
 * For use for simulation and test purposes only
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Marc Orr
 */

#ifndef __CL_COMMAND_QUEUE_HH__
#define __CL_COMMAND_QUEUE_HH__

#include <cstdint>
#include <cstdlib>

typedef struct signal_info{
    hsa_signal_t completion_signal;
    struct signal_info *next;
} signal_info_t;

class _cl_command_queue {
  public:
    _cl_command_queue()
    {
        numDispLeft = (volatile uint32_t*)calloc(1, sizeof(uint32_t));
        *numDispLeft = 0;
        signal_head = NULL;
    }

    void addSignal(hsa_signal_t csignal) {
        signal_info_t *temp = (signal_info_t *)malloc(sizeof(signal_info_t));
        temp->completion_signal.handle = csignal.handle;
        temp->next = signal_head;
        signal_head = temp;
    }

    void removeSignal(hsa_signal_t csignal) {
        signal_info_t *temp = signal_head;
        signal_info_t *prev = NULL;
        while(temp) {
            if(temp->completion_signal.handle == csignal.handle) {
                if(prev) {
                    prev->next = temp->next;
                    free(temp);
                } else {
                    signal_head = temp->next;
                    free(temp);
                }
                
            }
            prev = temp;
            temp = temp->next;
        }
    }

    void finish() {
        signal_info_t *temp = signal_head;
        while(temp) {
            hsa_signal_value_t *compSig = (hsa_signal_value_t *)temp->completion_signal.handle; 
            DPRINT("Waiting at: %p\n", compSig);
            while (!compSig[0]) {
                __builtin_ia32_monitor ((void *)compSig, 0, 0);
#if 1
                //__builtin_ia32_mwait(0,(event_list[i])->hsaTaskPtr->gdSize[0]);
                __builtin_ia32_mwait(0,1);
#else
                asm("hlt");
#endif
            }
            signal_info_t *prev = temp;
            temp = temp->next;
            free(prev);
        }
        signal_head = NULL;
    }

    cl_uint ID;
    volatile uint32_t *numDispLeft;
    signal_info_t *signal_head;
};

#endif // __CL_COMMAND_QUEUE_HH__
