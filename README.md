# HSA signal Working directory #

### Instructions for building HSAIL model ###

* Change to gem5
```
cd gem5
```
* Source the environment
```
source ./scripts/env.sh
```
* Build HSAIL
```
./buildHSA.sh
```

### Run hello world for multi-gpu ###

* Edit the following script as required and run it
```
./scripts/run_hello.sh
```



# Running OpenCL Benchmarks on gem5 HSAIL_X86 #

This repository contains the source of [HeteroMark]( https://github.com/NUCAR-DEV/Hetero-Mark) benchmarks and scripts needed to convert the kernel code to assembler code which can then be used as an input to HSAIL_X86 configuration in gem5.


It also contains a submodule link to gem5. So if you are cloning this repository to your local workspace, use the following command:


```
git clone --recursive git clone https://bitbucket.org/synergy-lab/gem5_heterogeneous.git
```


### Prerequisites ###

* gcc 
* python
* scons
* opencl-dev
* LLVM based high level compiler

```
git clone https://github.com/HSAFoundation/HSAIL-HLC-Stable.git
```

* gem5 - HSAIL_X86 setup ( This repo already contains a link)


### Running an OpenCL program ###
* Building HSAIL_X86

```
python `which scons` -j 24 build/HSAIL_X86/gem5.debug
```


* APU Configuration
    * The example configuration is given in configs/example/apu_se.py in gem5 repository


* Running a hello world on GPU

```
./build/HSAIL_X86/gem5.opt configs/example/apu_se.py -c tests/test-progs/gpu-hello/bin/x86/linux/gpu-hello -k tests/test-progs/gpu-hello/bin/x86/linux/gpu-hello-kernel.asm
```

* Compiling an OpenCL kernel to low level assembly

    All the scripts and binaries used are present in the HLC bin directory.

    ```
	$ clc2 --enable-hsail-extensions --cl-std=CL2.0 my_kernel.cl -o my_kernel.fe.bc
	$ llvm-link my_kernel.fe.bc �l builtins-hsail.bc �o my_kernel.linked.bc
	$ opt -O3 -gpu -whole -verify my_kernel.linked.bc -o my_kernel.opt.bc
	$ llc -filetype=obj -hsail-enable-gcn=0 -march=hsail-64 my_kernel.opt.bc -o my_kernel.asm
    ```


* Building the CPU binary

    Use make to build the CPU binary (TODO).

* To get human readable dissassembly from the assembly file.

    ```
	hsailasm -disassemble my_kernel.asm -o my_kernel.dasm
    ```
